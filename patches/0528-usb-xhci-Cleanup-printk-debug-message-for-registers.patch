From 38ab74d2df8639f0a4490a18f30b99381f04f480 Mon Sep 17 00:00:00 2001
From: Lu Baolu <baolu.lu@linux.intel.com>
Date: Fri, 8 Dec 2017 17:59:11 +0200
Subject: [PATCH 0528/1795] usb: xhci: Cleanup printk debug message for
 registers

The content of each register has been exposed through debugfs.
There is no need to dump register content with printk in code
lines. Remove them to make code more concise and readable.

Signed-off-by: Lu Baolu <baolu.lu@linux.intel.com>
Signed-off-by: Mathias Nyman <mathias.nyman@linux.intel.com>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
(cherry picked from commit 4c116cb138977838786257f039003eb76a05dc7a)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 drivers/usb/host/xhci-dbg.c | 243 ------------------------------------
 drivers/usb/host/xhci-mem.c |   4 -
 drivers/usb/host/xhci.c     |   6 -
 drivers/usb/host/xhci.h     |   5 -
 4 files changed, 258 deletions(-)

diff --git a/drivers/usb/host/xhci-dbg.c b/drivers/usb/host/xhci-dbg.c
index 584d7b9a3683..f20ef2ef1cb2 100644
--- a/drivers/usb/host/xhci-dbg.c
+++ b/drivers/usb/host/xhci-dbg.c
@@ -10,238 +10,6 @@
 
 #include "xhci.h"
 
-#define XHCI_INIT_VALUE 0x0
-
-/* Add verbose debugging later, just print everything for now */
-
-void xhci_dbg_regs(struct xhci_hcd *xhci)
-{
-	u32 temp;
-
-	xhci_dbg(xhci, "// xHCI capability registers at %p:\n",
-			xhci->cap_regs);
-	temp = readl(&xhci->cap_regs->hc_capbase);
-	xhci_dbg(xhci, "// @%p = 0x%x (CAPLENGTH AND HCIVERSION)\n",
-			&xhci->cap_regs->hc_capbase, temp);
-	xhci_dbg(xhci, "//   CAPLENGTH: 0x%x\n",
-			(unsigned int) HC_LENGTH(temp));
-	xhci_dbg(xhci, "//   HCIVERSION: 0x%x\n",
-			(unsigned int) HC_VERSION(temp));
-
-	xhci_dbg(xhci, "// xHCI operational registers at %p:\n", xhci->op_regs);
-
-	temp = readl(&xhci->cap_regs->run_regs_off);
-	xhci_dbg(xhci, "// @%p = 0x%x RTSOFF\n",
-			&xhci->cap_regs->run_regs_off,
-			(unsigned int) temp & RTSOFF_MASK);
-	xhci_dbg(xhci, "// xHCI runtime registers at %p:\n", xhci->run_regs);
-
-	temp = readl(&xhci->cap_regs->db_off);
-	xhci_dbg(xhci, "// @%p = 0x%x DBOFF\n", &xhci->cap_regs->db_off, temp);
-	xhci_dbg(xhci, "// Doorbell array at %p:\n", xhci->dba);
-}
-
-static void xhci_print_cap_regs(struct xhci_hcd *xhci)
-{
-	u32 temp;
-	u32 hci_version;
-
-	xhci_dbg(xhci, "xHCI capability registers at %p:\n", xhci->cap_regs);
-
-	temp = readl(&xhci->cap_regs->hc_capbase);
-	hci_version = HC_VERSION(temp);
-	xhci_dbg(xhci, "CAPLENGTH AND HCIVERSION 0x%x:\n",
-			(unsigned int) temp);
-	xhci_dbg(xhci, "CAPLENGTH: 0x%x\n",
-			(unsigned int) HC_LENGTH(temp));
-	xhci_dbg(xhci, "HCIVERSION: 0x%x\n", hci_version);
-
-	temp = readl(&xhci->cap_regs->hcs_params1);
-	xhci_dbg(xhci, "HCSPARAMS 1: 0x%x\n",
-			(unsigned int) temp);
-	xhci_dbg(xhci, "  Max device slots: %u\n",
-			(unsigned int) HCS_MAX_SLOTS(temp));
-	xhci_dbg(xhci, "  Max interrupters: %u\n",
-			(unsigned int) HCS_MAX_INTRS(temp));
-	xhci_dbg(xhci, "  Max ports: %u\n",
-			(unsigned int) HCS_MAX_PORTS(temp));
-
-	temp = readl(&xhci->cap_regs->hcs_params2);
-	xhci_dbg(xhci, "HCSPARAMS 2: 0x%x\n",
-			(unsigned int) temp);
-	xhci_dbg(xhci, "  Isoc scheduling threshold: %u\n",
-			(unsigned int) HCS_IST(temp));
-	xhci_dbg(xhci, "  Maximum allowed segments in event ring: %u\n",
-			(unsigned int) HCS_ERST_MAX(temp));
-
-	temp = readl(&xhci->cap_regs->hcs_params3);
-	xhci_dbg(xhci, "HCSPARAMS 3 0x%x:\n",
-			(unsigned int) temp);
-	xhci_dbg(xhci, "  Worst case U1 device exit latency: %u\n",
-			(unsigned int) HCS_U1_LATENCY(temp));
-	xhci_dbg(xhci, "  Worst case U2 device exit latency: %u\n",
-			(unsigned int) HCS_U2_LATENCY(temp));
-
-	temp = readl(&xhci->cap_regs->hcc_params);
-	xhci_dbg(xhci, "HCC PARAMS 0x%x:\n", (unsigned int) temp);
-	xhci_dbg(xhci, "  HC generates %s bit addresses\n",
-			HCC_64BIT_ADDR(temp) ? "64" : "32");
-	xhci_dbg(xhci, "  HC %s Contiguous Frame ID Capability\n",
-			HCC_CFC(temp) ? "has" : "hasn't");
-	xhci_dbg(xhci, "  HC %s generate Stopped - Short Package event\n",
-			HCC_SPC(temp) ? "can" : "can't");
-	/* FIXME */
-	xhci_dbg(xhci, "  FIXME: more HCCPARAMS debugging\n");
-
-	temp = readl(&xhci->cap_regs->run_regs_off);
-	xhci_dbg(xhci, "RTSOFF 0x%x:\n", temp & RTSOFF_MASK);
-
-	/* xhci 1.1 controllers have the HCCPARAMS2 register */
-	if (hci_version > 0x100) {
-		temp = readl(&xhci->cap_regs->hcc_params2);
-		xhci_dbg(xhci, "HCC PARAMS2 0x%x:\n", (unsigned int) temp);
-		xhci_dbg(xhci, "  HC %s Force save context capability",
-			 HCC2_FSC(temp) ? "supports" : "doesn't support");
-		xhci_dbg(xhci, "  HC %s Large ESIT Payload Capability",
-			 HCC2_LEC(temp) ? "supports" : "doesn't support");
-		xhci_dbg(xhci, "  HC %s Extended TBC capability",
-			 HCC2_ETC(temp) ? "supports" : "doesn't support");
-	}
-}
-
-static void xhci_print_command_reg(struct xhci_hcd *xhci)
-{
-	u32 temp;
-
-	temp = readl(&xhci->op_regs->command);
-	xhci_dbg(xhci, "USBCMD 0x%x:\n", temp);
-	xhci_dbg(xhci, "  HC is %s\n",
-			(temp & CMD_RUN) ? "running" : "being stopped");
-	xhci_dbg(xhci, "  HC has %sfinished hard reset\n",
-			(temp & CMD_RESET) ? "not " : "");
-	xhci_dbg(xhci, "  Event Interrupts %s\n",
-			(temp & CMD_EIE) ? "enabled " : "disabled");
-	xhci_dbg(xhci, "  Host System Error Interrupts %s\n",
-			(temp & CMD_HSEIE) ? "enabled " : "disabled");
-	xhci_dbg(xhci, "  HC has %sfinished light reset\n",
-			(temp & CMD_LRESET) ? "not " : "");
-}
-
-static void xhci_print_status(struct xhci_hcd *xhci)
-{
-	u32 temp;
-
-	temp = readl(&xhci->op_regs->status);
-	xhci_dbg(xhci, "USBSTS 0x%x:\n", temp);
-	xhci_dbg(xhci, "  Event ring is %sempty\n",
-			(temp & STS_EINT) ? "not " : "");
-	xhci_dbg(xhci, "  %sHost System Error\n",
-			(temp & STS_FATAL) ? "WARNING: " : "No ");
-	xhci_dbg(xhci, "  HC is %s\n",
-			(temp & STS_HALT) ? "halted" : "running");
-}
-
-static void xhci_print_op_regs(struct xhci_hcd *xhci)
-{
-	xhci_dbg(xhci, "xHCI operational registers at %p:\n", xhci->op_regs);
-	xhci_print_command_reg(xhci);
-	xhci_print_status(xhci);
-}
-
-static void xhci_print_ports(struct xhci_hcd *xhci)
-{
-	__le32 __iomem *addr;
-	int i, j;
-	int ports;
-	char *names[NUM_PORT_REGS] = {
-		"status",
-		"power",
-		"link",
-		"reserved",
-	};
-
-	ports = HCS_MAX_PORTS(xhci->hcs_params1);
-	addr = &xhci->op_regs->port_status_base;
-	for (i = 0; i < ports; i++) {
-		for (j = 0; j < NUM_PORT_REGS; j++) {
-			xhci_dbg(xhci, "%p port %s reg = 0x%x\n",
-					addr, names[j],
-					(unsigned int) readl(addr));
-			addr++;
-		}
-	}
-}
-
-void xhci_print_ir_set(struct xhci_hcd *xhci, int set_num)
-{
-	struct xhci_intr_reg __iomem *ir_set = &xhci->run_regs->ir_set[set_num];
-	void __iomem *addr;
-	u32 temp;
-	u64 temp_64;
-
-	addr = &ir_set->irq_pending;
-	temp = readl(addr);
-	if (temp == XHCI_INIT_VALUE)
-		return;
-
-	xhci_dbg(xhci, "  %p: ir_set[%i]\n", ir_set, set_num);
-
-	xhci_dbg(xhci, "  %p: ir_set.pending = 0x%x\n", addr,
-			(unsigned int)temp);
-
-	addr = &ir_set->irq_control;
-	temp = readl(addr);
-	xhci_dbg(xhci, "  %p: ir_set.control = 0x%x\n", addr,
-			(unsigned int)temp);
-
-	addr = &ir_set->erst_size;
-	temp = readl(addr);
-	xhci_dbg(xhci, "  %p: ir_set.erst_size = 0x%x\n", addr,
-			(unsigned int)temp);
-
-	addr = &ir_set->rsvd;
-	temp = readl(addr);
-	if (temp != XHCI_INIT_VALUE)
-		xhci_dbg(xhci, "  WARN: %p: ir_set.rsvd = 0x%x\n",
-				addr, (unsigned int)temp);
-
-	addr = &ir_set->erst_base;
-	temp_64 = xhci_read_64(xhci, addr);
-	xhci_dbg(xhci, "  %p: ir_set.erst_base = @%08llx\n",
-			addr, temp_64);
-
-	addr = &ir_set->erst_dequeue;
-	temp_64 = xhci_read_64(xhci, addr);
-	xhci_dbg(xhci, "  %p: ir_set.erst_dequeue = @%08llx\n",
-			addr, temp_64);
-}
-
-void xhci_print_run_regs(struct xhci_hcd *xhci)
-{
-	u32 temp;
-	int i;
-
-	xhci_dbg(xhci, "xHCI runtime registers at %p:\n", xhci->run_regs);
-	temp = readl(&xhci->run_regs->microframe_index);
-	xhci_dbg(xhci, "  %p: Microframe index = 0x%x\n",
-			&xhci->run_regs->microframe_index,
-			(unsigned int) temp);
-	for (i = 0; i < 7; i++) {
-		temp = readl(&xhci->run_regs->rsvd[i]);
-		if (temp != XHCI_INIT_VALUE)
-			xhci_dbg(xhci, "  WARN: %p: Rsvd[%i] = 0x%x\n",
-					&xhci->run_regs->rsvd[i],
-					i, (unsigned int) temp);
-	}
-}
-
-void xhci_print_registers(struct xhci_hcd *xhci)
-{
-	xhci_print_cap_regs(xhci);
-	xhci_print_op_regs(xhci);
-	xhci_print_ports(xhci);
-}
-
 void xhci_dbg_erst(struct xhci_hcd *xhci, struct xhci_erst *erst)
 {
 	u64 addr = erst->erst_dma_addr;
@@ -260,17 +28,6 @@ void xhci_dbg_erst(struct xhci_hcd *xhci, struct xhci_erst *erst)
 	}
 }
 
-void xhci_dbg_cmd_ptrs(struct xhci_hcd *xhci)
-{
-	u64 val;
-
-	val = xhci_read_64(xhci, &xhci->op_regs->cmd_ring);
-	xhci_dbg(xhci, "// xHC command ring deq ptr low bits + flags = @%08x\n",
-			lower_32_bits(val));
-	xhci_dbg(xhci, "// xHC command ring deq ptr high bits = @%08x\n",
-			upper_32_bits(val));
-}
-
 char *xhci_get_slot_state(struct xhci_hcd *xhci,
 		struct xhci_container_ctx *ctx)
 {
diff --git a/drivers/usb/host/xhci-mem.c b/drivers/usb/host/xhci-mem.c
index 7d6b33c89a2e..42247bf87b21 100644
--- a/drivers/usb/host/xhci-mem.c
+++ b/drivers/usb/host/xhci-mem.c
@@ -2469,7 +2469,6 @@ int xhci_mem_init(struct xhci_hcd *xhci, gfp_t flags)
 	xhci_dbg_trace(xhci, trace_xhci_dbg_init,
 			"// Setting command ring address to 0x%016llx", val_64);
 	xhci_write_64(xhci, val_64, &xhci->op_regs->cmd_ring);
-	xhci_dbg_cmd_ptrs(xhci);
 
 	xhci->lpm_command = xhci_alloc_command_with_ctx(xhci, true, flags);
 	if (!xhci->lpm_command)
@@ -2487,8 +2486,6 @@ int xhci_mem_init(struct xhci_hcd *xhci, gfp_t flags)
 			"// Doorbell array is located at offset 0x%x"
 			" from cap regs base addr", val);
 	xhci->dba = (void __iomem *) xhci->cap_regs + val;
-	xhci_dbg_regs(xhci);
-	xhci_print_run_regs(xhci);
 	/* Set ir_set to interrupt register set 0 */
 	xhci->ir_set = &xhci->run_regs->ir_set[0];
 
@@ -2532,7 +2529,6 @@ int xhci_mem_init(struct xhci_hcd *xhci, gfp_t flags)
 	xhci_set_hc_event_deq(xhci);
 	xhci_dbg_trace(xhci, trace_xhci_dbg_init,
 			"Wrote ERST address to ir_set 0.");
-	xhci_print_ir_set(xhci, 0);
 
 	/*
 	 * XXX: Might need to set the Interrupter Moderation Register to
diff --git a/drivers/usb/host/xhci.c b/drivers/usb/host/xhci.c
index b383a7774a6c..334c48f8abae 100644
--- a/drivers/usb/host/xhci.c
+++ b/drivers/usb/host/xhci.c
@@ -589,8 +589,6 @@ int xhci_run(struct usb_hcd *hcd)
 	if (ret)
 		return ret;
 
-	xhci_dbg_cmd_ptrs(xhci);
-
 	xhci_dbg(xhci, "ERST memory map follows:\n");
 	xhci_dbg_erst(xhci, &xhci->erst);
 	temp_64 = xhci_read_64(xhci, &xhci->ir_set->erst_dequeue);
@@ -621,7 +619,6 @@ int xhci_run(struct usb_hcd *hcd)
 			"// Enabling event ring interrupter %p by writing 0x%x to irq_pending",
 			xhci->ir_set, (unsigned int) ER_IRQ_ENABLE(temp));
 	writel(ER_IRQ_ENABLE(temp), &xhci->ir_set->irq_pending);
-	xhci_print_ir_set(xhci, 0);
 
 	if (xhci->quirks & XHCI_NEC_HOST) {
 		struct xhci_command *command;
@@ -701,7 +698,6 @@ static void xhci_stop(struct usb_hcd *hcd)
 	writel((temp & ~0x1fff) | STS_EINT, &xhci->op_regs->status);
 	temp = readl(&xhci->ir_set->irq_pending);
 	writel(ER_IRQ_DISABLE(temp), &xhci->ir_set->irq_pending);
-	xhci_print_ir_set(xhci, 0);
 
 	xhci_dbg_trace(xhci, trace_xhci_dbg_init, "cleaning up memory");
 	xhci_mem_cleanup(xhci);
@@ -1079,7 +1075,6 @@ int xhci_resume(struct xhci_hcd *xhci, bool hibernated)
 		writel((temp & ~0x1fff) | STS_EINT, &xhci->op_regs->status);
 		temp = readl(&xhci->ir_set->irq_pending);
 		writel(ER_IRQ_DISABLE(temp), &xhci->ir_set->irq_pending);
-		xhci_print_ir_set(xhci, 0);
 
 		xhci_dbg(xhci, "cleaning up memory\n");
 		xhci_mem_cleanup(xhci);
@@ -4913,7 +4908,6 @@ int xhci_gen_setup(struct usb_hcd *hcd, xhci_get_quirks_t get_quirks)
 	xhci->hcc_params = readl(&xhci->cap_regs->hcc_params);
 	if (xhci->hci_version > 0x100)
 		xhci->hcc_params2 = readl(&xhci->cap_regs->hcc_params2);
-	xhci_print_registers(xhci);
 
 	xhci->quirks |= quirks;
 
diff --git a/drivers/usb/host/xhci.h b/drivers/usb/host/xhci.h
index 2852ed52575b..9760e54ecd4d 100644
--- a/drivers/usb/host/xhci.h
+++ b/drivers/usb/host/xhci.h
@@ -1931,12 +1931,7 @@ static inline int xhci_link_trb_quirk(struct xhci_hcd *xhci)
 }
 
 /* xHCI debugging */
-void xhci_print_ir_set(struct xhci_hcd *xhci, int set_num);
-void xhci_print_registers(struct xhci_hcd *xhci);
-void xhci_dbg_regs(struct xhci_hcd *xhci);
-void xhci_print_run_regs(struct xhci_hcd *xhci);
 void xhci_dbg_erst(struct xhci_hcd *xhci, struct xhci_erst *erst);
-void xhci_dbg_cmd_ptrs(struct xhci_hcd *xhci);
 char *xhci_get_slot_state(struct xhci_hcd *xhci,
 		struct xhci_container_ctx *ctx);
 void xhci_dbg_trace(struct xhci_hcd *xhci, void (*trace)(struct va_format *),
-- 
2.19.0


From 847371fa592da758284c0e69cc434ef1265b5bc7 Mon Sep 17 00:00:00 2001
From: Sergei Shtylyov <sergei.shtylyov@cogentembedded.com>
Date: Thu, 3 May 2018 22:43:13 +0300
Subject: [PATCH 1432/1795] PCI: rcar: Factor out rcar_pcie_hw_init() call

rcar_pcie_hw_init_{h1|gen2|gen3}() only differ in the PCIe PHY init code
and all end with a call to rcar_pcie_hw_init(), thus it makes sense to
move that call into the driver's probe() method and then rename those
functions to rcar_pcie_phy_init_{h1|gen2|gen3}().

Signed-off-by: Sergei Shtylyov <sergei.shtylyov@cogentembedded.com>
[lorenzo.pieralisi@arm.com: updated commit log]
Signed-off-by: Lorenzo Pieralisi <lorenzo.pieralisi@arm.com>
Reviewed-by: Simon Horman <horms+renesas@verge.net.au>

(cherry picked from commit 9d5014e90e6d62eabe03946dbba27076b77b31e1)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 drivers/pci/host/pcie-rcar.c | 42 +++++++++++++++++++-----------------
 1 file changed, 22 insertions(+), 20 deletions(-)

diff --git a/drivers/pci/host/pcie-rcar.c b/drivers/pci/host/pcie-rcar.c
index 695465df4580..09b0b0af782c 100644
--- a/drivers/pci/host/pcie-rcar.c
+++ b/drivers/pci/host/pcie-rcar.c
@@ -629,7 +629,7 @@ static int rcar_pcie_hw_init(struct rcar_pcie *pcie)
 	return 0;
 }
 
-static int rcar_pcie_hw_init_h1(struct rcar_pcie *pcie)
+static int rcar_pcie_phy_init_h1(struct rcar_pcie *pcie)
 {
 	/* Initialize the phy */
 	phy_write_reg(pcie, 0, 0x42, 0x1, 0x0EC34191);
@@ -649,10 +649,10 @@ static int rcar_pcie_hw_init_h1(struct rcar_pcie *pcie)
 	phy_write_reg(pcie, 0, 0x64, 0x1, 0x3F0F1F0F);
 	phy_write_reg(pcie, 0, 0x66, 0x1, 0x00008000);
 
-	return rcar_pcie_hw_init(pcie);
+	return 0;
 }
 
-static int rcar_pcie_hw_init_gen2(struct rcar_pcie *pcie)
+static int rcar_pcie_phy_init_gen2(struct rcar_pcie *pcie)
 {
 	/*
 	 * These settings come from the R-Car Series, 2nd Generation User's
@@ -669,10 +669,10 @@ static int rcar_pcie_hw_init_gen2(struct rcar_pcie *pcie)
 	rcar_pci_write_reg(pcie, 0x00000001, GEN2_PCIEPHYCTRL);
 	rcar_pci_write_reg(pcie, 0x00000006, GEN2_PCIEPHYCTRL);
 
-	return rcar_pcie_hw_init(pcie);
+	return 0;
 }
 
-static int rcar_pcie_hw_init_gen3(struct rcar_pcie *pcie)
+static int rcar_pcie_phy_init_gen3(struct rcar_pcie *pcie)
 {
 	int err;
 
@@ -680,11 +680,7 @@ static int rcar_pcie_hw_init_gen3(struct rcar_pcie *pcie)
 	if (err)
 		return err;
 
-	err = phy_power_on(pcie->phy);
-	if (err)
-		return err;
-
-	return rcar_pcie_hw_init(pcie);
+	return phy_power_on(pcie->phy);
 }
 
 static int rcar_msi_alloc(struct rcar_msi *chip)
@@ -1073,17 +1069,18 @@ static int rcar_pcie_parse_map_dma_ranges(struct rcar_pcie *pcie,
 }
 
 static const struct of_device_id rcar_pcie_of_match[] = {
-	{ .compatible = "renesas,pcie-r8a7779", .data = rcar_pcie_hw_init_h1 },
+	{ .compatible = "renesas,pcie-r8a7779",
+	  .data = rcar_pcie_phy_init_h1 },
 	{ .compatible = "renesas,pcie-r8a7790",
-	  .data = rcar_pcie_hw_init_gen2 },
+	  .data = rcar_pcie_phy_init_gen2 },
 	{ .compatible = "renesas,pcie-r8a7791",
-	  .data = rcar_pcie_hw_init_gen2 },
+	  .data = rcar_pcie_phy_init_gen2 },
 	{ .compatible = "renesas,pcie-rcar-gen2",
-	  .data = rcar_pcie_hw_init_gen2 },
+	  .data = rcar_pcie_phy_init_gen2 },
 	{ .compatible = "renesas,pcie-r8a7795",
-	  .data = rcar_pcie_hw_init_gen3 },
+	  .data = rcar_pcie_phy_init_gen3 },
 	{ .compatible = "renesas,pcie-rcar-gen3",
-	  .data = rcar_pcie_hw_init_gen3 },
+	  .data = rcar_pcie_phy_init_gen3 },
 	{},
 };
 
@@ -1131,7 +1128,7 @@ static int rcar_pcie_probe(struct platform_device *pdev)
 	struct rcar_pcie *pcie;
 	unsigned int data;
 	int err;
-	int (*hw_init_fn)(struct rcar_pcie *);
+	int (*phy_init_fn)(struct rcar_pcie *);
 	struct pci_host_bridge *bridge;
 
 	bridge = pci_alloc_host_bridge(sizeof(*pcie));
@@ -1165,10 +1162,15 @@ static int rcar_pcie_probe(struct platform_device *pdev)
 	if (err)
 		goto err_pm_put;
 
-	/* Failure to get a link might just be that no cards are inserted */
-	hw_init_fn = of_device_get_match_data(dev);
-	err = hw_init_fn(pcie);
+	phy_init_fn = of_device_get_match_data(dev);
+	err = phy_init_fn(pcie);
 	if (err) {
+		dev_err(dev, "failed to init PCIe PHY\n");
+		goto err_pm_put;
+	}
+
+	/* Failure to get a link might just be that no cards are inserted */
+	if (rcar_pcie_hw_init(pcie)) {
 		dev_info(dev, "PCIe link down\n");
 		err = -ENODEV;
 		goto err_pm_put;
-- 
2.19.0


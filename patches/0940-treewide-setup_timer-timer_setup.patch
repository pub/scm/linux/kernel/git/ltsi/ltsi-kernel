From 2a8a53468474ce2182007ca4cb701b8280755abd Mon Sep 17 00:00:00 2001
From: Kees Cook <keescook@chromium.org>
Date: Mon, 16 Oct 2017 14:43:17 -0700
Subject: [PATCH 0940/1795] treewide: setup_timer() -> timer_setup()

This converts all remaining cases of the old setup_timer() API into using
timer_setup(), where the callback argument is the structure already
holding the struct timer_list. These should have no behavioral changes,
since they just change which pointer is passed into the callback with
the same available pointers after conversion. It handles the following
examples, in addition to some other variations.

Casting from unsigned long:

    void my_callback(unsigned long data)
    {
        struct something *ptr = (struct something *)data;
    ...
    }
    ...
    setup_timer(&ptr->my_timer, my_callback, ptr);

and forced object casts:

    void my_callback(struct something *ptr)
    {
    ...
    }
    ...
    setup_timer(&ptr->my_timer, my_callback, (unsigned long)ptr);

become:

    void my_callback(struct timer_list *t)
    {
        struct something *ptr = from_timer(ptr, t, my_timer);
    ...
    }
    ...
    timer_setup(&ptr->my_timer, my_callback, 0);

Direct function assignments:

    void my_callback(unsigned long data)
    {
        struct something *ptr = (struct something *)data;
    ...
    }
    ...
    ptr->my_timer.function = my_callback;

have a temporary cast added, along with converting the args:

    void my_callback(struct timer_list *t)
    {
        struct something *ptr = from_timer(ptr, t, my_timer);
    ...
    }
    ...
    ptr->my_timer.function = (TIMER_FUNC_TYPE)my_callback;

And finally, callbacks without a data assignment:

    void my_callback(unsigned long data)
    {
    ...
    }
    ...
    setup_timer(&ptr->my_timer, my_callback, 0);

have their argument renamed to verify they're unused during conversion:

    void my_callback(struct timer_list *unused)
    {
    ...
    }
    ...
    timer_setup(&ptr->my_timer, my_callback, 0);

The conversion is done with the following Coccinelle script:

spatch --very-quiet --all-includes --include-headers \
	-I ./arch/x86/include -I ./arch/x86/include/generated \
	-I ./include -I ./arch/x86/include/uapi \
	-I ./arch/x86/include/generated/uapi -I ./include/uapi \
	-I ./include/generated/uapi --include ./include/linux/kconfig.h \
	--dir . \
	--cocci-file ~/src/data/timer_setup.cocci

@fix_address_of@
expression e;
@@

 setup_timer(
-&(e)
+&e
 , ...)

// Update any raw setup_timer() usages that have a NULL callback, but
// would otherwise match change_timer_function_usage, since the latter
// will update all function assignments done in the face of a NULL
// function initialization in setup_timer().
@change_timer_function_usage_NULL@
expression _E;
identifier _timer;
type _cast_data;
@@

(
-setup_timer(&_E->_timer, NULL, _E);
+timer_setup(&_E->_timer, NULL, 0);
|
-setup_timer(&_E->_timer, NULL, (_cast_data)_E);
+timer_setup(&_E->_timer, NULL, 0);
|
-setup_timer(&_E._timer, NULL, &_E);
+timer_setup(&_E._timer, NULL, 0);
|
-setup_timer(&_E._timer, NULL, (_cast_data)&_E);
+timer_setup(&_E._timer, NULL, 0);
)

@change_timer_function_usage@
expression _E;
identifier _timer;
struct timer_list _stl;
identifier _callback;
type _cast_func, _cast_data;
@@

(
-setup_timer(&_E->_timer, _callback, _E);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E->_timer, &_callback, _E);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E->_timer, _callback, (_cast_data)_E);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E->_timer, &_callback, (_cast_data)_E);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E->_timer, (_cast_func)_callback, _E);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E->_timer, (_cast_func)&_callback, _E);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E->_timer, (_cast_func)_callback, (_cast_data)_E);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E->_timer, (_cast_func)&_callback, (_cast_data)_E);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E._timer, _callback, (_cast_data)_E);
+timer_setup(&_E._timer, _callback, 0);
|
-setup_timer(&_E._timer, _callback, (_cast_data)&_E);
+timer_setup(&_E._timer, _callback, 0);
|
-setup_timer(&_E._timer, &_callback, (_cast_data)_E);
+timer_setup(&_E._timer, _callback, 0);
|
-setup_timer(&_E._timer, &_callback, (_cast_data)&_E);
+timer_setup(&_E._timer, _callback, 0);
|
-setup_timer(&_E._timer, (_cast_func)_callback, (_cast_data)_E);
+timer_setup(&_E._timer, _callback, 0);
|
-setup_timer(&_E._timer, (_cast_func)_callback, (_cast_data)&_E);
+timer_setup(&_E._timer, _callback, 0);
|
-setup_timer(&_E._timer, (_cast_func)&_callback, (_cast_data)_E);
+timer_setup(&_E._timer, _callback, 0);
|
-setup_timer(&_E._timer, (_cast_func)&_callback, (_cast_data)&_E);
+timer_setup(&_E._timer, _callback, 0);
|
 _E->_timer@_stl.function = _callback;
|
 _E->_timer@_stl.function = &_callback;
|
 _E->_timer@_stl.function = (_cast_func)_callback;
|
 _E->_timer@_stl.function = (_cast_func)&_callback;
|
 _E._timer@_stl.function = _callback;
|
 _E._timer@_stl.function = &_callback;
|
 _E._timer@_stl.function = (_cast_func)_callback;
|
 _E._timer@_stl.function = (_cast_func)&_callback;
)

// callback(unsigned long arg)
@change_callback_handle_cast
 depends on change_timer_function_usage@
identifier change_timer_function_usage._callback;
identifier change_timer_function_usage._timer;
type _origtype;
identifier _origarg;
type _handletype;
identifier _handle;
@@

 void _callback(
-_origtype _origarg
+struct timer_list *t
 )
 {
(
	... when != _origarg
	_handletype *_handle =
-(_handletype *)_origarg;
+from_timer(_handle, t, _timer);
	... when != _origarg
|
	... when != _origarg
	_handletype *_handle =
-(void *)_origarg;
+from_timer(_handle, t, _timer);
	... when != _origarg
|
	... when != _origarg
	_handletype *_handle;
	... when != _handle
	_handle =
-(_handletype *)_origarg;
+from_timer(_handle, t, _timer);
	... when != _origarg
|
	... when != _origarg
	_handletype *_handle;
	... when != _handle
	_handle =
-(void *)_origarg;
+from_timer(_handle, t, _timer);
	... when != _origarg
)
 }

// callback(unsigned long arg) without existing variable
@change_callback_handle_cast_no_arg
 depends on change_timer_function_usage &&
                     !change_callback_handle_cast@
identifier change_timer_function_usage._callback;
identifier change_timer_function_usage._timer;
type _origtype;
identifier _origarg;
type _handletype;
@@

 void _callback(
-_origtype _origarg
+struct timer_list *t
 )
 {
+	_handletype *_origarg = from_timer(_origarg, t, _timer);
+
	... when != _origarg
-	(_handletype *)_origarg
+	_origarg
	... when != _origarg
 }

// Avoid already converted callbacks.
@match_callback_converted
 depends on change_timer_function_usage &&
            !change_callback_handle_cast &&
	    !change_callback_handle_cast_no_arg@
identifier change_timer_function_usage._callback;
identifier t;
@@

 void _callback(struct timer_list *t)
 { ... }

// callback(struct something *handle)
@change_callback_handle_arg
 depends on change_timer_function_usage &&
	    !match_callback_converted &&
            !change_callback_handle_cast &&
            !change_callback_handle_cast_no_arg@
identifier change_timer_function_usage._callback;
identifier change_timer_function_usage._timer;
type _handletype;
identifier _handle;
@@

 void _callback(
-_handletype *_handle
+struct timer_list *t
 )
 {
+	_handletype *_handle = from_timer(_handle, t, _timer);
	...
 }

// If change_callback_handle_arg ran on an empty function, remove
// the added handler.
@unchange_callback_handle_arg
 depends on change_timer_function_usage &&
	    change_callback_handle_arg@
identifier change_timer_function_usage._callback;
identifier change_timer_function_usage._timer;
type _handletype;
identifier _handle;
identifier t;
@@

 void _callback(struct timer_list *t)
 {
-	_handletype *_handle = from_timer(_handle, t, _timer);
 }

// We only want to refactor the setup_timer() data argument if we've found
// the matching callback. This undoes changes in change_timer_function_usage.
@unchange_timer_function_usage
 depends on change_timer_function_usage &&
            !change_callback_handle_cast &&
            !change_callback_handle_cast_no_arg &&
	    !change_callback_handle_arg@
expression change_timer_function_usage._E;
identifier change_timer_function_usage._timer;
identifier change_timer_function_usage._callback;
type change_timer_function_usage._cast_data;
@@

(
-timer_setup(&_E->_timer, _callback, 0);
+setup_timer(&_E->_timer, _callback, (_cast_data)_E);
|
-timer_setup(&_E._timer, _callback, 0);
+setup_timer(&_E._timer, _callback, (_cast_data)&_E);
)

// If we fixed a callback from a .function assignment, fix the
// assignment cast now.
@change_timer_function_assignment
 depends on change_timer_function_usage &&
            (change_callback_handle_cast ||
             change_callback_handle_cast_no_arg ||
             change_callback_handle_arg)@
expression change_timer_function_usage._E;
identifier change_timer_function_usage._timer;
identifier change_timer_function_usage._callback;
type _cast_func;
typedef TIMER_FUNC_TYPE;
@@

(
 _E->_timer.function =
-_callback
+(TIMER_FUNC_TYPE)_callback
 ;
|
 _E->_timer.function =
-&_callback
+(TIMER_FUNC_TYPE)_callback
 ;
|
 _E->_timer.function =
-(_cast_func)_callback;
+(TIMER_FUNC_TYPE)_callback
 ;
|
 _E->_timer.function =
-(_cast_func)&_callback
+(TIMER_FUNC_TYPE)_callback
 ;
|
 _E._timer.function =
-_callback
+(TIMER_FUNC_TYPE)_callback
 ;
|
 _E._timer.function =
-&_callback;
+(TIMER_FUNC_TYPE)_callback
 ;
|
 _E._timer.function =
-(_cast_func)_callback
+(TIMER_FUNC_TYPE)_callback
 ;
|
 _E._timer.function =
-(_cast_func)&_callback
+(TIMER_FUNC_TYPE)_callback
 ;
)

// Sometimes timer functions are called directly. Replace matched args.
@change_timer_function_calls
 depends on change_timer_function_usage &&
            (change_callback_handle_cast ||
             change_callback_handle_cast_no_arg ||
             change_callback_handle_arg)@
expression _E;
identifier change_timer_function_usage._timer;
identifier change_timer_function_usage._callback;
type _cast_data;
@@

 _callback(
(
-(_cast_data)_E
+&_E->_timer
|
-(_cast_data)&_E
+&_E._timer
|
-_E
+&_E->_timer
)
 )

// If a timer has been configured without a data argument, it can be
// converted without regard to the callback argument, since it is unused.
@match_timer_function_unused_data@
expression _E;
identifier _timer;
identifier _callback;
@@

(
-setup_timer(&_E->_timer, _callback, 0);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E->_timer, _callback, 0L);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E->_timer, _callback, 0UL);
+timer_setup(&_E->_timer, _callback, 0);
|
-setup_timer(&_E._timer, _callback, 0);
+timer_setup(&_E._timer, _callback, 0);
|
-setup_timer(&_E._timer, _callback, 0L);
+timer_setup(&_E._timer, _callback, 0);
|
-setup_timer(&_E._timer, _callback, 0UL);
+timer_setup(&_E._timer, _callback, 0);
|
-setup_timer(&_timer, _callback, 0);
+timer_setup(&_timer, _callback, 0);
|
-setup_timer(&_timer, _callback, 0L);
+timer_setup(&_timer, _callback, 0);
|
-setup_timer(&_timer, _callback, 0UL);
+timer_setup(&_timer, _callback, 0);
|
-setup_timer(_timer, _callback, 0);
+timer_setup(_timer, _callback, 0);
|
-setup_timer(_timer, _callback, 0L);
+timer_setup(_timer, _callback, 0);
|
-setup_timer(_timer, _callback, 0UL);
+timer_setup(_timer, _callback, 0);
)

@change_callback_unused_data
 depends on match_timer_function_unused_data@
identifier match_timer_function_unused_data._callback;
type _origtype;
identifier _origarg;
@@

 void _callback(
-_origtype _origarg
+struct timer_list *unused
 )
 {
	... when != _origarg
 }

Signed-off-by: Kees Cook <keescook@chromium.org>
(cherry picked from commit e99e88a9d2b067465adaa9c111ada99a041bef9a)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>

Conflicts:
	arch/alpha/kernel/srmcons.c
	arch/arm/mach-iop32x/n2100.c
	arch/arm/mach-orion5x/db88f5281-setup.c
	arch/blackfin/kernel/nmi.c
	arch/mips/lasat/picvue_proc.c
	arch/powerpc/kernel/tau_6xx.c
	arch/powerpc/oprofile/op_model_cell.c
	arch/powerpc/platforms/cell/spufs/sched.c
	arch/powerpc/platforms/powermac/low_i2c.c
	arch/s390/kernel/time.c
	arch/sh/drivers/heartbeat.c
	arch/sh/drivers/pci/common.c
	arch/sh/drivers/push-switch.c
	block/blk-stat.c
	block/blk-throttle.c
	drivers/atm/ambassador.c
	drivers/atm/firestream.c
	drivers/atm/horizon.c
	drivers/atm/idt77252.c
	drivers/atm/lanai.c
	drivers/atm/nicstar.c
	drivers/block/DAC960.c
	drivers/block/DAC960.h
	drivers/block/rsxx/dma.c
	drivers/block/skd_main.c
	drivers/block/sunvdc.c
	drivers/block/umem.c
	drivers/block/xsysace.c
	drivers/char/ipmi/bt-bmc.c
	drivers/char/ipmi/ipmi_msghandler.c
	drivers/char/ipmi/ipmi_si_intf.c
	drivers/char/ipmi/ipmi_ssif.c
	drivers/char/tpm/tpm-dev-common.c
	drivers/gpu/drm/drm_vblank.c
	drivers/gpu/drm/exynos/exynos_drm_vidi.c
	drivers/gpu/drm/i2c/tda998x_drv.c
	drivers/gpu/drm/msm/adreno/a5xx_preempt.c
	drivers/gpu/drm/msm/msm_gpu.c
	drivers/gpu/drm/omapdrm/dss/dsi.c
	drivers/gpu/drm/rockchip/rockchip_drm_psr.c
	drivers/gpu/drm/vgem/vgem_fence.c
	drivers/gpu/drm/via/via_dmablit.c
	drivers/hid/hid-appleir.c
	drivers/hid/hid-prodikeys.c
	drivers/hid/hid-wiimote-core.c
	drivers/iio/common/ssp_sensors/ssp_dev.c
	drivers/infiniband/hw/mlx5/mr.c
	drivers/input/gameport/gameport.c
	drivers/input/joystick/db9.c
	drivers/input/joystick/gamecon.c
	drivers/input/joystick/turbografx.c
	drivers/iommu/iova.c
	drivers/isdn/capi/capidrv.c
	drivers/isdn/divert/isdn_divert.c
	drivers/isdn/hardware/eicon/divasi.c
	drivers/isdn/hardware/mISDN/hfcmulti.c
	drivers/isdn/hardware/mISDN/hfcpci.c
	drivers/isdn/hardware/mISDN/mISDNisar.c
	drivers/isdn/i4l/isdn_common.c
	drivers/isdn/i4l/isdn_net.c
	drivers/isdn/i4l/isdn_ppp.c
	drivers/isdn/i4l/isdn_tty.c
	drivers/media/platform/s5p-mfc/s5p_mfc.c
	drivers/media/platform/sti/c8sectpfe/c8sectpfe-core.c
	drivers/media/platform/vim2m.c
	drivers/media/usb/au0828/au0828-dvb.c
	drivers/media/usb/au0828/au0828-video.c
	drivers/memstick/core/ms_block.c
	drivers/mfd/rtsx_usb.c
	drivers/mmc/core/host.c
	drivers/mtd/sm_ftl.c
	drivers/net/caif/caif_hsi.c
	drivers/net/dsa/mv88e6xxx/phy.c
	drivers/net/eql.c
	drivers/net/ethernet/adi/bfin_mac.c
	drivers/net/ethernet/agere/et131x.c
	drivers/net/ethernet/amazon/ena/ena_netdev.c
	drivers/net/ethernet/aquantia/atlantic/aq_nic.c
	drivers/net/ethernet/atheros/atl1c/atl1c_main.c
	drivers/net/ethernet/atheros/atl1e/atl1e_main.c
	drivers/net/ethernet/atheros/atlx/atl1.c
	drivers/net/ethernet/atheros/atlx/atl2.c
	drivers/net/ethernet/broadcom/b44.c
	drivers/net/ethernet/broadcom/bnx2.c
	drivers/net/ethernet/broadcom/bnx2x/bnx2x_main.c
	drivers/net/ethernet/broadcom/bnxt/bnxt.c
	drivers/net/ethernet/broadcom/tg3.c
	drivers/net/ethernet/cisco/enic/enic_main.c
	drivers/net/ethernet/marvell/mv643xx_eth.c
	drivers/net/ethernet/marvell/pxa168_eth.c
	drivers/net/ethernet/marvell/skge.c
	drivers/net/ethernet/marvell/sky2.c
	drivers/net/ethernet/myricom/myri10ge/myri10ge.c
	drivers/net/ethernet/oki-semi/pch_gbe/pch_gbe_main.c
	drivers/net/ethernet/pasemi/pasemi_mac.c
	drivers/net/ethernet/qlogic/qla3xxx.c
	drivers/net/ethernet/rocker/rocker_ofdpa.c
	drivers/net/ethernet/stmicro/stmmac/stmmac_main.c
	drivers/net/ethernet/synopsys/dwc-xlgmac-net.c
	drivers/net/ethernet/ti/cpsw_ale.c
	drivers/net/ethernet/ti/netcp_ethss.c
	drivers/net/ethernet/toshiba/spider_net.c
	drivers/net/slip/slip.c
	drivers/net/tun.c
	drivers/net/wan/hdlc_ppp.c
	drivers/net/wireless/broadcom/brcm80211/brcmfmac/btcoex.c
	drivers/net/wireless/broadcom/brcm80211/brcmfmac/cfg80211.c
	drivers/net/wireless/broadcom/brcm80211/brcmfmac/sdio.c
	drivers/net/wireless/intel/iwlwifi/dvm/main.c
	drivers/net/wireless/intel/iwlwifi/pcie/tx.c
	drivers/net/wireless/intersil/hostap/hostap_ap.c
	drivers/net/wireless/intersil/hostap/hostap_hw.c
	drivers/net/wireless/intersil/orinoco/orinoco_usb.c
	drivers/net/wireless/quantenna/qtnfmac/core.c
	drivers/net/wireless/ti/wlcore/main.c
	drivers/net/xen-netfront.c
	drivers/nfc/pn533/pn533.c
	drivers/nfc/st-nci/ndlc.c
	drivers/ntb/test/ntb_pingpong.c
	drivers/platform/x86/sony-laptop.c
	drivers/pps/clients/pps-ktimer.c
	drivers/rtc/rtc-dev.c
	drivers/s390/block/dasd.c
	drivers/s390/net/fsm.c
	drivers/scsi/arcmsr/arcmsr_hba.c
	drivers/scsi/arm/fas216.c
	drivers/scsi/bfa/bfad.c
	drivers/scsi/bfa/bfad_drv.h
	drivers/scsi/bnx2fc/bnx2fc_tgt.c
	drivers/scsi/esas2r/esas2r_main.c
	drivers/scsi/fcoe/fcoe_ctlr.c
	drivers/scsi/fnic/fnic_main.c
	drivers/scsi/ncr53c8xx.c
	drivers/staging/greybus/operation.c
	drivers/staging/lustre/lnet/lnet/net_fault.c
	drivers/staging/lustre/lustre/ptlrpc/service.c
	drivers/staging/media/imx/imx-ic-prpencvf.c
	drivers/staging/media/imx/imx-media-csi.c
	drivers/staging/most/hdm-usb/hdm_usb.c
	drivers/staging/rtl8192u/ieee80211/ieee80211_softmac.c
	drivers/staging/rtl8712/recv_linux.c
	drivers/staging/rtl8712/rtl8712_led.c
	drivers/staging/unisys/visorbus/visorbus_main.c
	drivers/staging/unisys/visornic/visornic_main.c
	drivers/staging/wilc1000/wilc_wfi_cfgoperations.c
	drivers/target/target_core_user.c
	drivers/tty/ipwireless/hardware.c
	drivers/tty/n_gsm.c
	drivers/tty/n_r3964.c
	drivers/tty/serial/crisv10.c
	drivers/tty/serial/fsl_lpuart.c
	drivers/tty/serial/ifx6x60.c
	drivers/tty/serial/imx.c
	drivers/tty/serial/kgdb_nmi.c
	drivers/tty/serial/max3100.c
	drivers/tty/serial/mux.c
	drivers/tty/serial/pnx8xxx_uart.c
	drivers/tty/serial/sa1100.c
	drivers/tty/serial/sh-sci.c
	drivers/tty/serial/sn_console.c
	drivers/tty/synclink.c
	drivers/tty/synclink_gt.c
	drivers/tty/synclinkmp.c
	drivers/usb/core/hcd.c
	drivers/usb/dwc2/hcd.c
	drivers/usb/dwc2/hcd_queue.c
	drivers/usb/gadget/udc/at91_udc.c
	drivers/usb/gadget/udc/dummy_hcd.c
	drivers/usb/gadget/udc/m66592-udc.c
	drivers/usb/gadget/udc/omap_udc.c
	drivers/usb/gadget/udc/pxa25x_udc.c
	drivers/usb/gadget/udc/r8a66597-udc.c
	drivers/usb/host/ohci-hcd.c
	drivers/usb/host/oxu210hp-hcd.c
	drivers/usb/host/r8a66597-hcd.c
	drivers/usb/host/sl811-hcd.c
	drivers/usb/host/uhci-hcd.c
	drivers/usb/host/uhci-q.c
	drivers/usb/host/xhci.c
	drivers/usb/serial/mos7840.c
	drivers/usb/storage/realtek_cr.c
	drivers/uwb/drp.c
	drivers/uwb/neh.c
	drivers/uwb/rsv.c
	drivers/uwb/uwb-internal.h
	drivers/watchdog/at91sam9_wdt.c
	drivers/watchdog/bcm47xx_wdt.c
	drivers/watchdog/bcm63xx_wdt.c
	drivers/watchdog/cpu5wdt.c
	drivers/watchdog/mpc8xxx_wdt.c
	drivers/watchdog/mtx-1_wdt.c
	drivers/watchdog/nuc900_wdt.c
	drivers/watchdog/pcwd.c
	drivers/watchdog/pika_wdt.c
	drivers/watchdog/rdc321x_wdt.c
	drivers/watchdog/shwdt.c
	fs/ocfs2/cluster/tcp.c
	kernel/padata.c
	kernel/time/clocksource.c
	net/802/garp.c
	net/802/mrp.c
	net/appletalk/aarp.c
	net/appletalk/ddp.c
	net/batman-adv/tp_meter.c
	net/bluetooth/hidp/core.c
	net/bluetooth/rfcomm/core.c
	net/bluetooth/sco.c
	net/core/drop_monitor.c
	net/core/gen_estimator.c
	net/core/neighbour.c
	net/decnet/dn_route.c
	net/decnet/dn_timer.c
	net/ipv4/igmp.c
	net/ipv4/ipmr.c
	net/ipv6/addrconf.c
	net/ipv6/ip6mr.c
	net/ipv6/mcast.c
	net/ncsi/ncsi-manage.c
	net/netfilter/nf_conntrack_expect.c
	net/netfilter/nfnetlink_log.c
	net/netfilter/xt_IDLETIMER.c
	net/netfilter/xt_LED.c
	net/nfc/nci/core.c
	net/rxrpc/call_object.c
	net/wireless/lib80211.c
	net/x25/x25_link.c
	net/xfrm/xfrm_state.c
---
 drivers/tty/serial/sh-sci.c |   16 +++++++---------
 1 file changed, 7 insertions(+), 9 deletions(-)

--- a/drivers/tty/serial/sh-sci.c
+++ b/drivers/tty/serial/sh-sci.c
@@ -1060,9 +1060,9 @@ static int scif_rtrg_enabled(struct uart
 			(SCFCR_RTRG0 | SCFCR_RTRG1)) != 0;
 }
 
-static void rx_fifo_timer_fn(unsigned long arg)
+static void rx_fifo_timer_fn(struct timer_list *t)
 {
-	struct sci_port *s = (struct sci_port *)arg;
+	struct sci_port *s = from_timer(s, t, rx_fifo_timer);
 	struct uart_port *port = &s->port;
 
 	dev_dbg(port->dev, "Rx timed out\n");
@@ -1140,8 +1140,7 @@ static ssize_t rx_fifo_timeout_store(str
 		sci->rx_fifo_timeout = r;
 		scif_set_rtrg(port, 1);
 		if (r > 0)
-			setup_timer(&sci->rx_fifo_timer, rx_fifo_timer_fn,
-				    (unsigned long)sci);
+			timer_setup(&sci->rx_fifo_timer, rx_fifo_timer_fn, 0);
 	}
 
 	return count;
@@ -1394,9 +1393,9 @@ static void work_fn_tx(struct work_struc
 	dma_async_issue_pending(chan);
 }
 
-static void rx_timer_fn(unsigned long arg)
+static void rx_timer_fn(struct timer_list *t)
 {
-	struct sci_port *s = (struct sci_port *)arg;
+	struct sci_port *s = from_timer(s, t, rx_timer);
 	struct dma_chan *chan = s->chan_rx;
 	struct uart_port *port = &s->port;
 	struct dma_tx_state state;
@@ -1574,7 +1573,7 @@ static void sci_request_dma(struct uart_
 			dma += s->buf_len_rx;
 		}
 
-		setup_timer(&s->rx_timer, rx_timer_fn, (unsigned long)s);
+		timer_setup(&s->rx_timer, rx_timer_fn, 0);
 
 		if (port->type == PORT_SCIFA || port->type == PORT_SCIFB)
 			sci_submit_rx(s);
@@ -2242,8 +2241,7 @@ static void sci_reset(struct uart_port *
 	if (s->rx_trigger > 1) {
 		if (s->rx_fifo_timeout) {
 			scif_set_rtrg(port, 1);
-			setup_timer(&s->rx_fifo_timer, rx_fifo_timer_fn,
-				    (unsigned long)s);
+			timer_setup(&s->rx_fifo_timer, rx_fifo_timer_fn, 0);
 		} else {
 			if (port->type == PORT_SCIFA ||
 			    port->type == PORT_SCIFB)

From 0c8a4aadaeef8a7285610ea591b52bdfe4cc7d85 Mon Sep 17 00:00:00 2001
From: Lu Baolu <baolu.lu@linux.intel.com>
Date: Thu, 5 Oct 2017 11:21:44 +0300
Subject: [PATCH 0250/1795] usb: xhci: Remove xhci->mutex from xhci_alloc_dev()

xhci->mutex was added in xhci_alloc_dev()  to protect two race sources
(xhci->slot_id and xhci->addr_dev) by commit a00918d0521d ("usb: host:
xhci: add mutex for non-thread-safe data").

While xhci->slot_id has been discarded in commit c2d3d49bba08 ("usb:
xhci: move slot_id from xhci_hcd to xhci_command structure"), and
xhci->addr_dev has been removed in commit 87e44f2aac8d ("usb: xhci:
remove the use of xhci->addr_dev"), it's now safe to remove the use of
xhci->mutex in xhci_alloc_dev().

Link: https://marc.info/?l=linux-usb&m=150306294725821&w=2

Suggested-by: Mathias Nyman <mathias.nyman@linux.intel.com>
Signed-off-by: Lu Baolu <baolu.lu@linux.intel.com>
Signed-off-by: Mathias Nyman <mathias.nyman@linux.intel.com>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
(cherry picked from commit 089ed4904e948f93a82ebf62b068ae55aec852ab)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 drivers/usb/host/xhci.c | 4 ----
 1 file changed, 4 deletions(-)

diff --git a/drivers/usb/host/xhci.c b/drivers/usb/host/xhci.c
index eda3af130d2c..b64c55324916 100644
--- a/drivers/usb/host/xhci.c
+++ b/drivers/usb/host/xhci.c
@@ -3714,13 +3714,10 @@ int xhci_alloc_dev(struct usb_hcd *hcd, struct usb_device *udev)
 	if (!command)
 		return 0;
 
-	/* xhci->slot_id and xhci->addr_dev are not thread-safe */
-	mutex_lock(&xhci->mutex);
 	spin_lock_irqsave(&xhci->lock, flags);
 	ret = xhci_queue_slot_control(xhci, command, TRB_ENABLE_SLOT, 0);
 	if (ret) {
 		spin_unlock_irqrestore(&xhci->lock, flags);
-		mutex_unlock(&xhci->mutex);
 		xhci_dbg(xhci, "FIXME: allocate a command ring segment\n");
 		xhci_free_command(xhci, command);
 		return 0;
@@ -3730,7 +3727,6 @@ int xhci_alloc_dev(struct usb_hcd *hcd, struct usb_device *udev)
 
 	wait_for_completion(command->completion);
 	slot_id = command->slot_id;
-	mutex_unlock(&xhci->mutex);
 
 	if (!slot_id || command->status != COMP_SUCCESS) {
 		xhci_err(xhci, "Error while assigning device slot ID\n");
-- 
2.19.0


From 6d547f3d524fda1f98d3805045bce8825217a2cb Mon Sep 17 00:00:00 2001
From: Claudio Foellmi <claudio.foellmi@ergon.ch>
Date: Thu, 5 Oct 2017 14:44:14 +0200
Subject: [PATCH 0404/1795] i2c: generic recovery: check SCL before SDA

Move the check for a stuck SCL before the check for a high SDA.
This prevent false positives in the specific case that SDA is fine
and SCL is stuck, which previously returned 0.

Also check SDA again after the loop, if we can.
Together, these changes should lead to a lot more failed
recoveries being caught and returning error codes.

Signed-off-by: Claudio Foellmi <claudio.foellmi@ergon.ch>
Signed-off-by: Wolfram Sang <wsa@the-dreams.de>
(cherry picked from commit 1f35b8653687b9c08c8d58489c1b5cb9cf961c17)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 drivers/i2c/i2c-core-base.c | 10 +++++++---
 1 file changed, 7 insertions(+), 3 deletions(-)

diff --git a/drivers/i2c/i2c-core-base.c b/drivers/i2c/i2c-core-base.c
index 7b961c9c62ef..6334f8837371 100644
--- a/drivers/i2c/i2c-core-base.c
+++ b/drivers/i2c/i2c-core-base.c
@@ -205,9 +205,6 @@ static int i2c_generic_recovery(struct i2c_adapter *adap)
 	 */
 	while (i++ < RECOVERY_CLK_CNT * 2) {
 		if (val) {
-			/* Break if SDA is high */
-			if (bri->get_sda && bri->get_sda(adap))
-					break;
 			/* SCL shouldn't be low here */
 			if (!bri->get_scl(adap)) {
 				dev_err(&adap->dev,
@@ -215,6 +212,9 @@ static int i2c_generic_recovery(struct i2c_adapter *adap)
 				ret = -EBUSY;
 				break;
 			}
+			/* Break if SDA is high */
+			if (bri->get_sda && bri->get_sda(adap))
+				break;
 		}
 
 		val = !val;
@@ -222,6 +222,10 @@ static int i2c_generic_recovery(struct i2c_adapter *adap)
 		ndelay(RECOVERY_NDELAY);
 	}
 
+	/* check if recovery actually succeeded */
+	if (bri->get_sda && !bri->get_sda(adap))
+		ret = -EBUSY;
+
 	if (bri->unprepare_recovery)
 		bri->unprepare_recovery(adap);
 
-- 
2.19.0


From fefd0395d34b151629d6cc717b62bb91206a0f73 Mon Sep 17 00:00:00 2001
From: Magnus Damm <damm+renesas@opensource.se>
Date: Mon, 18 Sep 2017 15:46:42 +0200
Subject: [PATCH 0168/1795] clocksource/drivers/sh_cmt: Use 0x3f mask for
 SH_CMT_48BIT case

Always use 0x3f as channel mask for the SH_CMT_48BIT type of devices.
Once this patch is applied the "renesas,channels-mask" property will
be ignored by the driver for older devices matching SH_CMT_48BIT. In
the future when all CMT types store channel mask in the driver then
we will be able to deprecate and remove "renesas,channels-mask" from DTS.

Signed-off-by: Magnus Damm <damm+renesas@opensource.se>
Acked-by: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
Reviewed-by: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
Signed-off-by: Daniel Lezcano <daniel.lezcano@linaro.org>
(cherry picked from commit 464eed841f54b56df35132434497235f06b154f6)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 drivers/clocksource/sh_cmt.c | 14 +++++++++++---
 1 file changed, 11 insertions(+), 3 deletions(-)

diff --git a/drivers/clocksource/sh_cmt.c b/drivers/clocksource/sh_cmt.c
index e09e8bf0bb9b..c104c80424c8 100644
--- a/drivers/clocksource/sh_cmt.c
+++ b/drivers/clocksource/sh_cmt.c
@@ -74,6 +74,8 @@ enum sh_cmt_model {
 struct sh_cmt_info {
 	enum sh_cmt_model model;
 
+	unsigned int channels_mask;
+
 	unsigned long width; /* 16 or 32 bit version of hardware block */
 	unsigned long overflow_bit;
 	unsigned long clear_bits;
@@ -212,6 +214,7 @@ static const struct sh_cmt_info sh_cmt_info[] = {
 	},
 	[SH_CMT_48BIT] = {
 		.model = SH_CMT_48BIT,
+		.channels_mask = 0x3f,
 		.width = 32,
 		.overflow_bit = SH_CMT32_CMCSR_CMF,
 		.clear_bits = ~(SH_CMT32_CMCSR_CMF | SH_CMT32_CMCSR_OVF),
@@ -966,9 +969,14 @@ static int sh_cmt_setup(struct sh_cmt_device *cmt, struct platform_device *pdev)
 		id = of_match_node(sh_cmt_of_table, pdev->dev.of_node);
 		cmt->info = id->data;
 
-		ret = sh_cmt_parse_dt(cmt);
-		if (ret < 0)
-			return ret;
+		/* prefer in-driver channel configuration over DT */
+		if (cmt->info->channels_mask) {
+			cmt->hw_channels = cmt->info->channels_mask;
+		} else {
+			ret = sh_cmt_parse_dt(cmt);
+			if (ret < 0)
+				return ret;
+		}
 	} else if (pdev->dev.platform_data) {
 		struct sh_timer_config *cfg = pdev->dev.platform_data;
 		const struct platform_device_id *id = pdev->id_entry;
-- 
2.19.0


From 0b0a368ec3f116562422f43a5d991ab9e606b29f Mon Sep 17 00:00:00 2001
From: Masahiro Yamada <yamada.masahiro@socionext.com>
Date: Thu, 18 Jan 2018 01:28:03 +0900
Subject: [PATCH 0470/1795] mmc: tmio: move {tmio_}mmc_of_parse() to
 tmio_mmc_host_alloc()

mmc_of_parse() parses various DT properties and sets capability flags
accordingly.  However, drivers have no chance to run platform init
code depending on such flags because mmc_of_parse() is called from
tmio_mmc_host_probe().

Move mmc_of_parse() to tmio_mmc_host_alloc() so that drivers can
handle capabilities before mmc_add_host().  Move tmio_mmc_of_parse()
likewise.

Signed-off-by: Masahiro Yamada <yamada.masahiro@socionext.com>
Reviewed-by: Wolfram Sang <wsa+renesas@sang-engineering.com>
Signed-off-by: Ulf Hansson <ulf.hansson@linaro.org>
(cherry picked from commit 6fb294f791af8f491812d4eef6b13a57c9c1de34)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 drivers/mmc/host/tmio_mmc_core.c | 19 +++++++++++++------
 1 file changed, 13 insertions(+), 6 deletions(-)

diff --git a/drivers/mmc/host/tmio_mmc_core.c b/drivers/mmc/host/tmio_mmc_core.c
index 411c6111e267..679bd96f4c14 100644
--- a/drivers/mmc/host/tmio_mmc_core.c
+++ b/drivers/mmc/host/tmio_mmc_core.c
@@ -1152,6 +1152,7 @@ struct tmio_mmc_host *tmio_mmc_host_alloc(struct platform_device *pdev,
 	struct mmc_host *mmc;
 	struct resource *res;
 	void __iomem *ctl;
+	int ret;
 
 	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
 	ctl = devm_ioremap_resource(&pdev->dev, res);
@@ -1170,8 +1171,20 @@ struct tmio_mmc_host *tmio_mmc_host_alloc(struct platform_device *pdev,
 	host->ops = tmio_mmc_ops;
 	mmc->ops = &host->ops;
 
+	ret = mmc_of_parse(host->mmc);
+	if (ret) {
+		host = ERR_PTR(ret);
+		goto free;
+	}
+
+	tmio_mmc_of_parse(pdev, pdata);
+
 	platform_set_drvdata(pdev, host);
 
+	return host;
+free:
+	mmc_free_host(mmc);
+
 	return host;
 }
 EXPORT_SYMBOL_GPL(tmio_mmc_host_alloc);
@@ -1198,15 +1211,9 @@ int tmio_mmc_host_probe(struct tmio_mmc_host *_host,
 	if (mmc->f_min == 0)
 		return -EINVAL;
 
-	tmio_mmc_of_parse(pdev, pdata);
-
 	if (!(pdata->flags & TMIO_MMC_HAS_IDLE_WAIT))
 		_host->write16_hook = NULL;
 
-	ret = mmc_of_parse(mmc);
-	if (ret < 0)
-		return ret;
-
 	_host->set_pwr = pdata->set_pwr;
 	_host->set_clk_div = pdata->set_clk_div;
 
-- 
2.19.0


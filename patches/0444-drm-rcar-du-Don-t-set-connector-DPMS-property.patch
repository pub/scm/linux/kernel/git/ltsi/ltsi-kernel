From 848186f058e2e86385e8a2508464df6b14806bfb Mon Sep 17 00:00:00 2001
From: Laurent Pinchart <laurent.pinchart+renesas@ideasonboard.com>
Date: Tue, 15 Aug 2017 15:36:19 +0300
Subject: [PATCH 0444/1795] drm: rcar-du: Don't set connector DPMS property

Since commit 4a97a3da420b ("drm: Don't update property values for atomic
drivers") atomic drivers must not update property values as properties
are read from the state instead. To catch remaining users, the
drm_object_property_set_value() function now throws a warning when
called by atomic drivers on non-immutable properties, and we hit that
warning when creating connectors.

The easy fix is to just remove the drm_object_property_set_value() as it
is used here to set the initial value of the connector's DPMS property
to OFF. The DPMS property applies on top of the connector's state crtc
pointer (initialized to NULL) that is the main connector on/off control,
and should thus default to ON.

Fixes: 4a97a3da420b ("drm: Don't update property values for atomic drivers")
Signed-off-by: Laurent Pinchart <laurent.pinchart+renesas@ideasonboard.com>
Reviewed-by: Daniel Vetter <daniel.vetter@ffwll.ch>
(cherry picked from commit e2f930aaa3519865bbdfa1a37d4974a717c1fa09)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 drivers/gpu/drm/rcar-du/rcar_du_lvdscon.c | 4 ----
 1 file changed, 4 deletions(-)

diff --git a/drivers/gpu/drm/rcar-du/rcar_du_lvdscon.c b/drivers/gpu/drm/rcar-du/rcar_du_lvdscon.c
index b373ad48ef5f..e96f2df0c305 100644
--- a/drivers/gpu/drm/rcar-du/rcar_du_lvdscon.c
+++ b/drivers/gpu/drm/rcar-du/rcar_du_lvdscon.c
@@ -79,10 +79,6 @@ int rcar_du_lvds_connector_init(struct rcar_du_device *rcdu,
 
 	drm_connector_helper_add(connector, &connector_helper_funcs);
 
-	connector->dpms = DRM_MODE_DPMS_OFF;
-	drm_object_property_set_value(&connector->base,
-		rcdu->ddev->mode_config.dpms_property, DRM_MODE_DPMS_OFF);
-
 	ret = drm_mode_connector_attach_encoder(connector, encoder);
 	if (ret < 0)
 		return ret;
-- 
2.19.0


From 1120188e35e783fab93874b8631720f9dfbe1804 Mon Sep 17 00:00:00 2001
From: Bartosz Golaszewski <brgl@bgdev.pl>
Date: Thu, 28 Dec 2017 11:49:10 +0100
Subject: [PATCH 0413/1795] dt-bindings: at24: consistently document the
 compatible property

Current description of the compatible property for at24 is quite vague.

State explicitly that any "<manufacturer>,<model>" pair is accepted as
long as a correct fallback is used for non-atmel chips.

Signed-off-by: Bartosz Golaszewski <brgl@bgdev.pl>
Reviewed-by: Rob Herring <robh@kernel.org>
(cherry picked from commit 6da28acf745fe2537d77b9044f78a2a4e100c773)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 .../devicetree/bindings/eeprom/at24.txt       | 58 ++++++++++++-------
 1 file changed, 36 insertions(+), 22 deletions(-)

diff --git a/Documentation/devicetree/bindings/eeprom/at24.txt b/Documentation/devicetree/bindings/eeprom/at24.txt
index cbc80e194ac6..5ac18ce2e8cd 100644
--- a/Documentation/devicetree/bindings/eeprom/at24.txt
+++ b/Documentation/devicetree/bindings/eeprom/at24.txt
@@ -2,28 +2,42 @@ EEPROMs (I2C)
 
 Required properties:
 
-  - compatible : should be "<manufacturer>,<type>", like these:
-
-	"atmel,24c00", "atmel,24c01", "atmel,24c02", "atmel,24c04",
-	"atmel,24c08", "atmel,24c16", "atmel,24c32", "atmel,24c64",
-	"atmel,24c128", "atmel,24c256", "atmel,24c512", "atmel,24c1024"
-
-	"catalyst,24c32"
-
-	"microchip,24c128"
-
-	"ramtron,24c64"
-
-	"renesas,r1ex24002"
-
-	The following manufacturers values have been deprecated:
-	"at", "at24"
-
-	 If there is no specific driver for <manufacturer>, a generic
-	 device with <type> and manufacturer "atmel" should be used.
-	 Possible types are:
-	 "24c00", "24c01", "24c02", "24c04", "24c08", "24c16", "24c32", "24c64",
-	 "24c128", "24c256", "24c512", "24c1024", "spd"
+  - compatible: Must be a "<manufacturer>,<model>" pair. The following <model>
+                values are supported (assuming "atmel" as manufacturer):
+
+                "atmel,24c00",
+                "atmel,24c01",
+                "atmel,24c02",
+                "atmel,spd",
+                "atmel,24c04",
+                "atmel,24c08",
+                "atmel,24c16",
+                "atmel,24c32",
+                "atmel,24c64",
+                "atmel,24c128",
+                "atmel,24c256",
+                "atmel,24c512",
+                "atmel,24c1024",
+
+                If <manufacturer> is not "atmel", then a fallback must be used
+                with the same <model> and "atmel" as manufacturer.
+
+                Example:
+                        compatible = "microchip,24c128", "atmel,24c128";
+
+                Supported manufacturers are:
+
+                "catalyst",
+                "microchip",
+                "ramtron",
+                "renesas",
+                "nxp",
+                "st",
+
+                Some vendors use different model names for chips which are just
+                variants of the above. Known such exceptions are listed below:
+
+                "renesas,r1ex24002" - the fallback is "atmel,24c02"
 
   - reg : the I2C address of the EEPROM
 
-- 
2.19.0

